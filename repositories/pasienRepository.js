const Pasien = require('../models/pasien');
const DB = require('../config/database');
class PasienRepository {
    constructor() {
        this.pasien = Pasien;
    }

    prepared(){
        return this.pasien.prepared()
    }

    async builder(query) {
        return await this.pasien.builder(query);
    }

    async findAll() {
        return await this.pasien.findAll();
    }

    async findById(id) {
        return await this.pasien.findById(id)
    }

    async find(filter) {
        return await this.pasien.find(filter);
    }

    async create(data) {
        return await this.pasien.create(data);
    }

    async update(id, data) {
        return await this.pasien.update(id, data);
    }

    async destroy(id) {
        return await this.pasien.destroy(id);
    }

    async countGender() {
        const query = this.prepared().select('jenis_kelamin').count('id as total').groupBy('jenis_kelamin');
        return await this.builder(query);
    }

    async countStatsBpjsAndUmum() {
        let result = {
            bpjs: [],
            umum: []
        };

        for (let i = 1; i <= 12; i++) {
            let queryBpjs = await DB('kunjungans')
                .whereRaw('MONTH(kunjungans.created_at) = ?', [i])
                .whereNotNull('pasiens.no_bpjs')
                .count('kunjungans.id as total')
                .leftJoin('pasiens', 'pasiens.id', 'kunjungans.pasien_id');

            let queryUmum = await DB('kunjungans')
                .whereRaw('MONTH(kunjungans.created_at) = ?', [i])
                .whereNull('pasiens.no_bpjs')
                .count('kunjungans.id as total')
                .leftJoin('pasiens', 'pasiens.id', 'kunjungans.pasien_id');


            console.log(`Bulan ${i} : ${queryBpjs[0].total} BPJS, ${queryUmum[0].total} Umum`);

            result.bpjs.push(queryBpjs[0].total);
            result.umum.push(queryUmum[0].total);
        }

        return result;
    }

}

module.exports = new PasienRepository();