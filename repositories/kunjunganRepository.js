const Kunjungan = require('../models/kunjungan');

class KunjunganRepository {
    constructor() {
        this.kunjungan = Kunjungan;
    }

    prepared() {
        return this.kunjungan.prepared()
    }

    async builder(query) {
        return await this.kunjungan.builder(query);
    }

    async findAll() {
        const query = this.prepared().select('kunjungans.*', 'pasiens.nama as nama_pasien')
            .leftJoin('pasiens', 'pasiens.id', 'kunjungans.pasien_id')
            .orderBy('kunjungans.id', 'desc');

        return await this.builder(query);
    }

    async findById(id) {
        const queryDataPasien = this.prepared().select(
            'kunjungans.id',
            'kunjungans.tanggal_kunjungan',
            'kunjungans.keluhan',
            'pasiens.nama as nama_pasien',
            'pasiens.alamat as alamat_pasien',
            'pasiens.tanggal_lahir as tanggal_lahir_pasien',
            'pasiens.jenis_kelamin as jenis_kelamin_pasien',
            'pasiens.no_ktp as no_ktp_pasien',
            'pasiens.no_hp as no_hp_pasien',
            'pasiens.no_bpjs as no_bpjs_pasien',
        )
            .leftJoin('pasiens', 'pasiens.id', 'kunjungans.pasien_id')
            .where('kunjungans.id', id);

        const countDetailsKunjungan = await this.builder(this.prepared().count('detail_kunjungans.id as total')
            .leftJoin('detail_kunjungans', 'detail_kunjungans.kunjungan_id', 'kunjungans.id')
            .where('kunjungans.id', id)
        );

        const dataPasien = await this.builder(queryDataPasien);
        let detailsKunjunganMap = [];

        if (countDetailsKunjungan[0].total > 0) {
            const queryDetailsKunjungan = this.prepared().select(
                'detail_kunjungans.id',
                'detail_kunjungans.kunjungan_id',
                'detail_kunjungans.poli_id',
                'detail_kunjungans.apotek_id',
                'detail_kunjungans.diagnosa',
                'detail_kunjungans.resep'
            )
                .leftJoin('detail_kunjungans', 'detail_kunjungans.kunjungan_id', 'kunjungans.id')
                .where('kunjungans.id', id);

            const detailsKunjungan = await this.builder(queryDetailsKunjungan);

            detailsKunjunganMap = detailsKunjungan.map(detail => {
                return {
                    id: detail.id,
                    kunjungan_id: detail.kunjungan_id,
                    tujuan: (detail.poli_id) ? 'Rawat Jalan' : 'Apotek',
                    diagnosa: detail.diagnosa,
                    resep: detail.resep
                };
            });
        }

        return {
            ...dataPasien[0],
            details_kunjungan: detailsKunjunganMap
        };
    }


    async find(filter) {
        return await this.kunjungan.find(filter);
    }

    async create(data) {
        return await this.kunjungan.create(data);
    }

    async update(id, data) {
        return await this.kunjungan.update(id, data);
    }

    async destroy(id) {
        return await this.kunjungan.destroy(id);
    }
}

module.exports = new KunjunganRepository();