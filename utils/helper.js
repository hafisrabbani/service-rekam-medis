exports.pagination = (data, page, limit, total) => ({
  data,
  page,
  limit,
  total
});