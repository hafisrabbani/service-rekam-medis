const { handleBadRequest,handleCustomError, handleNotFound } = require('../middlewares/error/errorMiddleware');

exports.BadRequest = (errorMessage) => {
    return (error, req, res, next) => {
        handleBadRequest(req, res, next);
    }
};

exports.CustomError = (errorMessage) => {
    return (error, req, res, next) => {
        handleCustomError(errorMessage, req, res, next);
    }
};

exports.NotFound = (errorMessage) => {
    return (error, req, res, next) => {
        handleNotFound(req, res, next);
    }
}



