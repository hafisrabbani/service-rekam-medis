const pasienRepository = require('../repositories/pasienRepository');


class PasienService {
    constructor() {
        this.pasienRepository = pasienRepository;
    }

    prepared() {
        return this.pasienRepository.prepared()
    }

    async builder(query) {
        return await this.pasienRepository.builder(query);
    }
    async findAll() {
        return await this.pasienRepository.findAll();
    }

    async findById(id) {
        return await this.pasienRepository.findById(id);
    }

    async find(filter) {
        return await this.pasienRepository.find(filter);
    }

    async create(data) {
        return await this.pasienRepository.create(data);
    }

    async update(id, data) {
        return await this.pasienRepository.update(id, data);
    }

    async destroy(id) {
        return await this.pasienRepository.destroy(id);
    }

    async countGender() {
        return await this.pasienRepository.countGender();
    }

    async countBPJSAndUmum(){
        return await  this.pasienRepository.countStatsBpjsAndUmum();
    }
}

module.exports = new PasienService();