const kunjunganRepository = require('../repositories/kunjunganRepository');


class KunjunganService {
    constructor() {
        this.kunjunganRepository = kunjunganRepository;
    }

    prepared() {
        return this.kunjunganRepository.prepared()
    }

    async builder(query) {
        return await this.kunjunganRepository.builder(query);
    }
    async findAll() {
        return await this.kunjunganRepository.findAll();
    }

    async findById(id) {
        return await this.kunjunganRepository.findById(id);
    }

    async find(filter) {
        return await this.kunjunganRepository.find(filter);
    }

    async create(data) {
        return await this.kunjunganRepository.create(data);
    }

    async update(id, data) {
        return await this.kunjunganRepository.update(id, data);
    }

    async destroy(id) {
        return await this.kunjunganRepository.destroy(id);
    }
}

module.exports = new KunjunganService();