const router = require('express').Router();

const {
    findAll,
    findById,
    update,
    create,
    destroy
} = require('../controllers/pasienController');

router.get('/', findAll);
router.get('/:id', findById);
router.post('/', create);
router.patch('/:id', update);
router.delete('/:id', destroy);

exports.routes = router;