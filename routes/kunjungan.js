const router = require('express').Router();
const {
    create,
    findAll,
    findById
} = require('../controllers/kunjunganController');

router.get('/', findAll);
router.post('/', create);
router.get('/:id', findById);

exports.routes = router;