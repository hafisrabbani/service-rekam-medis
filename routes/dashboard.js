const router = require('express').Router();
const {getDashboardStats} = require('../controllers/pasienController')

router.get('/', getDashboardStats)
exports.routes = router;