const errorResponse = (res, status, error) => {
    if (process.env.APP_ENV === 'production') {
        return res.status(status).json({
            message: 'Internal server error',
        });
    }

    return res.status(status).json({
        message: error.message,
        error: res.app.get('env') === 'development' ? error : {},
    });
};

const customErrorHandler = (status = 500) => (errorType, errorMessage) => {
    const customError = {
        type: errorType,
        message: errorMessage,
    };

    return (err, req, res, next) => {
        errorResponse(res, status, customError);
    };
};

module.exports = {
    errorResponse,
    customErrorHandler,
};
