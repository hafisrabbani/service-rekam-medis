const { customErrorHandler } = require('./errorHandler');

const handleErrors = (err, req, res, next) => {
    console.error(err.stack);
    return res.status(500).json({ message: 'Internal Server Error' });
};

const handleNotFound = (req, res, next) => {
    return res.status(404).json({ message: 'Not Found' });
};

const handleBadRequest = (req, res, next) => {
    return res.status(400).json({ message: 'Bad Request' });
}

const handleCustomError = (err, req, res, next) => {
    return res.status(err.status).json({ message: err.message });
}

const handleUnauthorized = (req, res, next) => {
    return res.status(401).json({ message: 'Unauthorized' });
}

module.exports = {
    handleErrors,
    handleNotFound,
    customErrorHandler,
    handleBadRequest,
    handleCustomError,
};
