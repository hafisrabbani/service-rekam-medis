const knexInstance = require("../config/database");
class BaseModel {
    constructor({ knex = knexInstance, name = '', tableName = '', selectableProps = [], timeout = 1000 }) {
        this.knex = knex;
        this.name = name;
        this.tableName = tableName;
        this.selectableProps = selectableProps;
        this.timeout = timeout;
    }

    prepared() {
        return this.knex(this.tableName).timeout(this.timeout);
    }

    async builder(queryBuilder) {
        try {
            return await queryBuilder;
        } catch (error) {
            throw new Error(`Failed to execute custom query: ${error.message}`);
        }
    }

    async create(props) {
        try {
            delete props.id;
            const result = await this.knex.insert(props)
                .returning(this.selectableProps)
                .into(this.tableName)
                .timeout(this.timeout);
            return result[0];
        } catch (error) {
            throw new Error(`Failed to create ${this.name}: ${error.message}`);
        }
    }

    async findAll() {
        try {
            return await this.knex.select(this.selectableProps)
                .from(this.tableName)
                .timeout(this.timeout);
        } catch (error) {
            throw new Error(`Failed to find all ${this.name}s: ${error.message}`);
        }
    }

    async find(filters) {
        try {
            return await this.knex.select(this.selectableProps)
                .from(this.tableName)
                .where(filters)
                .timeout(this.timeout);
        } catch (error) {
            throw new Error(`Failed to find ${this.name}s with filters: ${error.message}`);
        }
    }

    async findById(id){
        try {
            return await this.knex.select(this.selectableProps)
                .from(this.tableName)
                .where({ id })
                .timeout(this.timeout);
        } catch (error) {
            throw new Error(`Failed to find ${this.name} with ID ${id}: ${error.message}`);
        }
    }

    async update(id, props) {
        try {
            delete props.id;
            const result = await this.knex.update(props)
                .from(this.tableName)
                .where({ id })
                .returning(this.selectableProps)
                .timeout(this.timeout);
            return result[0];
        } catch (error) {
            throw new Error(`Failed to update ${this.name} with ID ${id}: ${error.message}`);
        }
    }

    async destroy(id) {
        try {
            return await this.knex.del()
                .from(this.tableName)
                .where({ id })
                .timeout(this.timeout);
        } catch (error) {
            throw new Error(`Failed to delete ${this.name} with ID ${id}: ${error.message}`);
        }
    }
}

module.exports = BaseModel;