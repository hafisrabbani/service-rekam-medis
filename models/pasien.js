const baseModel = require('./baseModel');

class Pasien extends baseModel {
    constructor() {
        super({
            name: 'Pasien',
            tableName: 'pasiens',
            selectableProps: [
                'id',
                'nama',
                'jenis_kelamin',
                'tanggal_lahir',
                'no_hp',
                'alamat',
                'no_bpjs',
                'no_ktp',
                'created_at',
                'updated_at']
        });
    }
}

module.exports = new Pasien();