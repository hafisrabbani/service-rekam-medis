const baseModel = require('./baseModel');

class Kunjungan extends baseModel {
    constructor() {
        super({
            name: 'Kunjungan',
            tableName: 'kunjungans',
            selectableProps: [
                'id',
                'nama',
                'jenis_kelamin',
                'tanggal_lahir',
                'no_hp',
                'alamat',
                'no_bpjs',
                'no_ktp',
                'created_at',
                'updated_at']
        });
    }
}

module.exports = new Kunjungan;