const {BadRequest, CustomError, NotFound} = require('../utils/error');
const DB = require('../config/database');
const KunjunganService = require('../services/kunjunganService');
const moment = require('moment');

exports.create = async (req, res, next) => {
    try {
        const {pasien_id, tanggal_kunjungan, keluhan} = req.body;
        if(!pasien_id || !tanggal_kunjungan || !keluhan){
            return BadRequest('Pasien, tanggal kunjungan, dan keluhan harus diisi')(null,req, res, next);
        }

        await DB.transaction(async (trx) => {
            const insert = await KunjunganService.create({
                pasien_id,
                tanggal_kunjungan: moment(tanggal_kunjungan).format('YYYY-MM-DD'),
                keluhan,
                created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
            });


            const detailKunjungan = await trx('detail_kunjungans').insert({
                kunjungan_id: insert,
                pembayaran: 30000,
                created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
            }).returning('*');

            await trx('invoices').insert({
                id_detail_kunjungan: detailKunjungan[0],
                created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
                updated_at: moment().format('YYYY-MM-DD HH:mm:ss')
            });

            await trx.commit();
        });


        return res.status(200).json({
            message: 'Success create kunjungan'
        })

    } catch (error) {
        next(error);
    }
}

exports.findAll = async (req, res, next) => {
    try {
        const kunjungans = await KunjunganService.findAll();
        res.status(200).json({
            message: 'success',
            data: kunjungans
        });
    } catch (error) {
        next(error);
    }
}

exports.findById = async (req, res, next) => {
    try {
        const {id} = req.params;
        const kunjungan = await KunjunganService.findById(id);
        res.status(200).json({
            message: 'success',
            data: kunjungan
        });
    } catch (error) {
        next(error);
    }
}

