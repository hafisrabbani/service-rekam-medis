const {BadRequest, CustomError, NotFound} = require('../utils/error');
const DB = require('../config/database');
const PasienService = require('../services/pasienService');
const moment = require('moment');
exports.findAll = async (req, res, next) => {
    try {
        const pasiens = await PasienService.findAll();

        return res.status(200).json({
            message: 'Success',
            data: pasiens,
        });
    } catch (error) {
        next(error);
    }
};

exports.findById = async (req, res, next) => {
    try {
        const {id} = req.params;
        if (!id) {
            return BadRequest('ID is required')(null, req, res, next);
        }

        const pasien = await PasienService.findById(id);
        if (pasien.length === 0) {
            return NotFound('Pasien not found')(null, req, res, next);
        }

        return res.status(200).json({
            message: 'Success',
            data: pasien
        });
    } catch (error) {
        next(error);
    }
}

exports.create = async (req, res, next) => {
    try {
        const {nama, alamat, tanggal_lahir, jenis_kelamin, no_hp, no_ktp, no_bpjs} = req.body;
        if (!nama || !alamat || !tanggal_lahir || !jenis_kelamin || !no_hp || !no_ktp) {
            return BadRequest('Nama, alamat, tanggal_lahir, jenis_kelamin, no_hp is required')(null, req, res, next);
        }
        if (await PasienService.builder(
            PasienService.prepared().where('no_ktp', no_ktp)
                .whereNotNull('no_bpjs', no_bpjs)
                .orWhere('no_hp', no_hp)
                .first()
        )) {
            return CustomError({
                status: 409,
                message: 'No KTP, No BPJS, No HP already exist',
            })(null, req, res, next);
        }

        await PasienService.create({
            nama,
            alamat,
            tanggal_lahir,
            jenis_kelamin,
            no_hp,
            no_ktp,
            no_bpjs,
            created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
            updated_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        });

        return res.status(200).json({
            message: 'Success create pasien',
        });
    } catch (error) {
        next(error);
    }
}

exports.update = async (req, res, next) => {
    try {
        const {id} = req.params;
        const {nama, alamat, tanggal_lahir, jenis_kelamin, no_hp, no_ktp, no_bpjs} = req.body;
        if (!id || !nama || !alamat || !tanggal_lahir || !jenis_kelamin || !no_hp || !no_ktp) {
            return BadRequest('ID, nama, alamat, tanggal_lahir, jenis_kelamin, no_hp is required')(null, req, res, next);
        }

        if (!await DB('pasiens').where('id', id).first()) {
            return NotFound('Pasien not found')(null, req, res, next);
        }
        const check = await PasienService.builder(
            PasienService.prepared().where('id', '!=', id)
                .andWhere(function () {
                    this.where('no_ktp', no_ktp)
                        .whereNotNull('no_bpjs', no_bpjs)
                        .orWhere('no_hp', no_hp);
                })
                .first()
        );
        console.log(check)
        if (check) {
            return CustomError({
                status: 409,
                message: 'No KTP, No BPJS, No HP already exists',
            })(null, req, res, next);
        }

        await PasienService.update(id, {
            nama,
            alamat,
            tanggal_lahir,
            jenis_kelamin,
            no_hp,
            no_ktp,
            no_bpjs,
            updated_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        });

        return res.status(200).json({
            message: 'Success update pasien',
        });
    } catch (error) {
        next(error);
    }
}

exports.destroy = async (req, res, next) => {
    try {
        const {id} = req.params;
        if (!id) {
            return BadRequest('ID is required')(null, req, res, next);
        }

        await PasienService.destroy(id);

        return res.status(200).json({
            message: 'Success delete pasien',
        });
    } catch (error) {
        next(error);
    }
}

exports.getDashboardStats = async (req,res,next) => {
    try{
        const dataGender = await PasienService.countGender();
        const dataBpjsAndNonBpjs = await PasienService.countBPJSAndUmum();
        return res.status(200).json({
            message: 'Success',
            data: {
                stat_gender: dataGender,
                stat_bpjs_permonth: dataBpjsAndNonBpjs
            }
        });

    }catch (error) {
        next(error);
    }
}