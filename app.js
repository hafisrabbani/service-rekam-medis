const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(cors());

const { handleErrors, handleNotFound } = require('./middlewares/error/errorMiddleware');


app.use('/api/v1', require('./routes/test').routes);
app.use('/api/v1/pasien', require('./routes/pasien').routes);
app.use('/api/v1/kunjungan', require('./routes/kunjungan').routes);
app.use('/api/v1/dashboard', require('./routes/dashboard').routes);
app.use(handleNotFound);
app.use(handleErrors);

module.exports = app;
